import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import CarouselSlide from '../Slide/CarouselSlide'
import './CarouselSlider.scss'

const Slider = () => {
  const slides = [
    {
      title: 'CINE CLÁSICO',
      description: 'Descubre uno de los clásicos del cine con el preestreno de la nueva película de Star Wars',
    },
    {
      title: 'OFERTA: Esta semana envío gratuito a partir de 30 euros',
      description: 'Envíos sujetos a todos los territorios del interior de la península. Quedan exentos por tanto las islas Baleares y Canarias',
    }
  ]
  return (
    <div className="carousel">
    <Carousel className="carousel-wrapper" showArrows={true} showThumbs={false}>
      { slides.map(({title, description}, index) =>
      <div className={`carousel-slider carousel-slide-${index+1}`} key={`slide-${index}`}>
        <div className="wrapper">
          <CarouselSlide
            title={title}
            description={description}
          />
        </div>
      </div>
      )}
    </Carousel>
  </div>
  )
}

export default Slider
