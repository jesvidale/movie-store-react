import PropTypes from 'prop-types'
import './CarouselSlide.scss'

const CarouselSlide = ({title, description}) => {
  return (
    <div className="carousel-slide-content">
      <div className="carousel-slide-info">
        <h2 className="carousel-slide-title">{ title }</h2>
        <div className="carousel-slide-description">
          { description }
        </div>
      </div>
    </div>
  )
}

CarouselSlide.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
}

export default CarouselSlide
