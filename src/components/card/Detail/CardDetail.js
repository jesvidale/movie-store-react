import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { addToOrder } from '../../../actions/order'
import './CardDetail.scss';

const CardDetail = ({productId, title, overview, releasedOn, vote, categories, adults, productionCountries, revenue, status}) => {
  const dispatch = useDispatch()
  return (
    <div className="card-detail-wrapper">
      <div className="card-detail">
        <div className="card-information">
          <span className="card-title">{ title }</span>
          <div className="card-information-rate">
            <ion-icon name="star" class="card-information-rate-icon"></ion-icon>
            <span className="card-information-rate-value">{ vote }</span>
          </div>
          <div className="card-information-overview">
            <p>{ overview } </p>
          </div>
        </div>
        <div className="card-information-related">
          { releasedOn ? <div className="card-information-section">
            <span className="card-information-related-title">Fecha de Lanzamiento:</span>
            <span className="card-information-related-content">{ releasedOn }</span>
          </div>: ''
          }
          <div className="card-information-section">
            <span className="card-information-related-title">Paises de lanzamiento:</span>
            { productionCountries.map((country, key) =>
            <span
              key={`country-${key}`}
              className="card-information-category"
            >{ key ? ', ': ''}{ country.name }</span>
            )}
          </div>
          <div className="card-information-section">
            <span className="card-information-related-title">Ingresos:</span>
            <span className="card-information-related-content">{ revenue } dólares</span>
          </div>
          <div className="card-information-section">
            <span className="card-information-related-title">Géneros:</span>
            { categories.map((category, index) =>
            <span
              key={`category-${index}`}
              className="card-information-category"
            >{ index ? ', ': ''}{ category.name }</span>
            )}
          </div>
        </div>
        <div className="button-group">
          <button className="button button-primary" onClick={() => dispatch(addToOrder({title, id: productId}))}>Añadir al carrito</button>
        </div>
      </div>
    </div>
  )
}

CardDetail.propTypes = {
  productId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  overview: PropTypes.string,
  releasedOn: PropTypes.string,
  vote: PropTypes.number.isRequired,
  categories: PropTypes.arrayOf(PropTypes.shape({
    to: PropTypes.string,
    title: PropTypes.string
  })),
  adults: PropTypes.bool,
  productionCountries: PropTypes.array,
  revenue: PropTypes.number,
  status: PropTypes.string
}

CardDetail.defaultProps = {
  productId: 0,
  title: '',
  overview: '',
  releasedOn: '',
  vote: 0,
  categories: [],
  productionCountries: [],
  revenue: 0,
  status: 'Released'
}

export default CardDetail
