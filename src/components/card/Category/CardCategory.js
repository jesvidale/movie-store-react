import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import './CardCategory.scss';

const CardCategory = ({categoryTitle, categoryId }) => {
  return (
    <Link
      className="card-categorie-url"
      to={ `catalogo/${categoryId}` }>
    <div className="card-categorie-wrapper">
      <span className="card-categorie-title">{ categoryTitle }</span>
    </div>
    </Link>
  )
}

CardCategory.propTypes = {
  categoryTitle: PropTypes.string.isRequired,
  categoryId: PropTypes.number.isRequired
}

export default CardCategory
