import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { addToOrder } from '../../../actions/order'
import './CardProduct.scss';

const CardProduct = ({productId, title, overview, releasedOn, vote, categories}) => {
  const categoriesDictionary = useSelector(state => state.categories.list)
  const dispatch = useDispatch()
  const getCategoryName = categoryID => {
    const res = categoriesDictionary.find(el => el.id === categoryID)
    return res ? res.name: 'uncategorized'
  }
  return (
    <div className="card-resume-wrapper">
    <div className="card-resume">
      <div className="card-information">
        <span className="card-title">{ title }</span>
        <Vote vote={vote}/>
        <div className="card-information-overview">
          <p>{ overview } </p>
        </div>
      </div>
      <div className="card-information-categories">
      <span className="card-information-categories-title">Géneros:</span>
      { categories ?
        <>
          { categories.map((category, index) =>
            <span
              key={`category-${index}`}
              className="card-information-category"
            >
            { index ? ', ': ' '}{ getCategoryName(category) }
          </span>
        )}
        </>:<></>
      }
      </div>
      <div className="button-group">
        <Link
          to={`/catalogo/pelicula/${productId}`}
          no-prefetch="true"
          className="button button-secondary"
        >
          Ver detalle
        </Link>
        <button className="button button-primary" onClick={() => dispatch(addToOrder({title, id: productId}))}>Añadir al carrito</button>
      </div>
    </div>
  </div>
  )
}

const Vote = ({vote}) => {
  return vote ? (
    <div className="card-information-rate">
      <ion-icon name="star" class="card-information-rate-icon"></ion-icon>
      <span className="card-information-rate-value">{ vote }</span>
    </div>
  ): ''
}

CardProduct.propTypes = {
  productId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  overview: PropTypes.string,
  releasedOn: PropTypes.string,
  vote: PropTypes.number.isRequired,
  categories: PropTypes.array
}

CardProduct.defaultProps = {
  productId: 1,
  title: '',
  vote: 0
}

export default CardProduct
