
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Navigation from '../Navigation/CommonNavigation'
import './CommonHeader.scss';

const Header = () => {
  const orderSize = useSelector(state => state.order.size)
  return (
    <div className="header">
      <div className="wrapper">
        <div className="header-container">
          <h1 className="sr-only">Movie store react</h1>
          <div className="cart-logo">
            <Link to={'/pedido'} title="pedido" className="order-link">
              <ion-icon name="cart-outline" class="order-icon"></ion-icon>
              <span className="cart-order-size">{orderSize}</span>
            </Link>
          </div>
          <Navigation />
        </div>
      </div>
    </div>
  )
}

export default Header;

