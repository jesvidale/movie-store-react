import './SearchForm.scss'

const SearchForm = (props) => {
  return (
    <form className="search-form">
    { props.children }
    <div className="form-fields-wrapper">
      <div className="form-field">
        <label htmlFor="moviename" className="sr-only">Nombre de la pelicula</label>
        <input id="moviename" name="moviename" placeholder="Nombre de la película" autoComplete="on" type="text" value={props.query} onChange={e => props.onQueryChange(e.target.value)} className="form-input"/>
      </div>
      <button type="submit" className="button button-primary button-submit" onClick={e => props.triggerSearch(props.query, e)}>Buscar</button>
    </div>
  </form>
  )
}

export default SearchForm
