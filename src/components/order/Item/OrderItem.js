import PropTypes from 'prop-types'
import './OrderItem.scss'

const OrderItem = ({id = 0, amount = 1, name = '', removeProduct}) => {
  return (
    <div className="order-item">
    <span className="order-item-title">{ name }</span>
    <div className="order-item-handler">
      <div className="order-item-amount-wrapper">
        <button
          className="button button-secondary button-amount"
          // @click="$emit('decreaseProduct', name, id)"
        > - </button>
        <span className="order-item-amount-value">{ amount }</span>
        <button
          className="button button-secondary button-amount"
          // @click="$emit('addProduct', name, id)"
        > + </button>
      </div>
      <button
        className="button button-primary button-trash"
        onClick={() => removeProduct(id)}
      >
        <span className="sr-only">Eliminar</span>
        <ion-icon name="trash-outline" title="Eliminar" class="button-trash-icon"></ion-icon>
      </button>
    </div>
  </div>
  )
}

OrderItem.propTypes = {
  id: PropTypes.number.isRequired,
  amount: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired
}

export default OrderItem
