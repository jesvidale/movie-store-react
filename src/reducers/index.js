import { combineReducers } from 'redux';

import { categories } from './categories';
import { order } from './order';

export default combineReducers({
  categories,
  order
});
