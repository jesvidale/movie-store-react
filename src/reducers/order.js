const initialState = {
  list: [],
  size: 0
}


export const order = (state = initialState, action) => {
  switch (action.type) {
    case 'DECREASE_EXISTING_ORDER':
      if(Object.prototype.hasOwnProperty.call(action.item, 'id')) {
        const element = state.list.find(el => el.id === action.item.id)
        if (element.amount > 1) {
          return {
            ...state,
            list: action.order.list.find(el => el.id === action.item.id) - 1,
            size: action.order - 1
          }
        }
      }
      break
    case 'ADD_TO_ORDER':
      if(Object.prototype.hasOwnProperty.call(action.item, 'id')) {
        if (state.list.some(el => el.id === action.item.id)) {
          return {
            ...state,
            list: state.list.find(el => el.id === action.item.id) + 1,
            size: state + 1
          }
        } else {
          return {
            ...state,
            list: [...state.list, {id: action.item.id, title: action.item.title, amount: 1}],
            size: state.size + 1
          }
        }
      }
      break
    case 'REMOVE_FROM_ORDER':
      if(Object.prototype.hasOwnProperty.call(action.item, 'id')) {
        if (state.list.find(el => el.id === action.item.id)) {
          const removeItem = state.list.find(el => el.id === action.item.id)
          return {
            ...state,
            list: state.list.filter(el => el.id !== removeItem.id),
            size: state.size - removeItem.amount
          }
        }
      }
      break
    default:
      return { ...state }
  }
}
