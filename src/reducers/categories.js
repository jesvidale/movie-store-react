const initialState = {
  list: [],
  size: 0
}

export const categories = (state = initialState, action) => {
  switch (action.type) {
    case 'REFRESH_CATEGORIES':
      return action.categories.length ?
        {
          ...state,
          list: action.categories,
          size: action.categories.length
        } : { ...state }
    default:
      return { ...state }
  }
}

