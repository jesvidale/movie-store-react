
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Main stylesheet
import './index.scss';

// Redux dependencies
import { Provider } from 'react-redux';
import { getAllCategories } from './actions/categories';
import store from './store';

// Main components imports
import Home from './pages/index/Home';
import Catalog from './pages/catalog/Catalog';
import Category from './pages/category/Category';
import Film from './pages/film/Film';
import Order from './pages/order/Order';
import Notfound from './pages/error/Notfound';

// Import Report
import reportWebVitals from './reportWebVitals';

store.dispatch(getAllCategories())

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/catalogo">
            <Catalog />
          </Route>
          <Route exact path="/catalogo/:categoryId">
            <Category />
          </Route>
          <Route exact path="/catalogo/pelicula/:productId">
            <Film />
          </Route>
          <Route exact path="/pedido">
            <Order />
          </Route>
          <Route>
            <Notfound />
          </Route>
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
