import CommonFooter from  '../../components/common/Footer/CommonFooter'
import CommonHeader from  '../../components/common/Header/CommonHeader'
import './PageWrapper.scss';

const PageWrapper = (props) => (
  <div className="page-wrapper">
    <CommonHeader />
    <div className="main-content">
      { props.children }
    </div>
    <CommonFooter />
  </div>
)

export default PageWrapper;
