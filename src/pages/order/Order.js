import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import Breadcrumb from '../../components/common/Breadcrumb/CommonBreadcrumb'
import OrderItem from '../../components/order/Item/OrderItem'
import { removeFromOrder } from '../../actions/order'
import './Order.scss'

import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

const Order = () => {
  const order = useSelector(state => state.order)
  const dispatch = useDispatch()
  const breadcrumbs = [
    {
      title: 'Pedido',
    }
  ]
  const removeProduct = id => {
    dispatch(removeFromOrder({ id }))
  }
  return (
    <PageWrapper>
      <div className="page-wrapper">
        <div className="section-wrapper">
          <div className="wrapper">
            <Breadcrumb breadcrumbs={breadcrumbs} />
            <h2 className="page-title">Resumen del pedido</h2>
            { !order.size ? 
            <div className="order-section">
              <div className="order-empty">
                <picture className="order-empty-image">
                  <source srcSet={'/empty_cart_mobile.webp'} alt="Pedido sin artículos" media="(max-width: 767px)"/>
                  <source srcSet={'/empty_cart_desktop.webp'} title="Pedido sin artículos" alt="Pedido sin artículos"/>
                  <img src={'/empty_cart_desktop.webp'} title="Pedido sin artículos" alt="Pedido sin artículos"/>
                </picture>
                <div className="order-empty-text">Pedido sin artículos, carrito vacío</div>
                <Link className="order-empty-link" to='/catalogo'>Ir a Catálogo</Link>
              </div>
            </div>
            :
            <div className="order">
              <ul className="order-summary">
                { order.list.map((item, key) =>
                  <li
                  key={`order-${key}`}
                  className="order-summary-item"
                >
                  <OrderItem
                    id={item.id}
                    amount={item.amount}
                    name={item.title}
                    removeProduct={removeProduct}
                  />
                </li>
                )}
              </ul>
              <button className="button button-primary button-summary">Continuar con el pago</button>
            </div>
          }
        </div>
      </div>
    </div>
  </PageWrapper>
  )
}

export default Order
