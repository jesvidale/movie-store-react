import { useState, useEffect  } from 'react'

// components
import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import CarouselSlider from '../../components/carousel/Slider/CarouselSlider'
import CardProduct from '../../components/card/Product/CardProduct'
import './Home.scss';

// services
import { getMostPopular } from '../../services/movies'

const Home = () => {
  const [popular, setPopular] = useState([])
  async function fetchMovies() {
    setPopular(await getMostPopular())
  }
  useEffect(() => {
    fetchMovies()
  }, [])
  return (
    <PageWrapper>
      <div className="section-wrapper section-introduction">
        <CarouselSlider />
      </div>
      <div className="section-wrapper">
        <div className="wrapper">
          <h2 className="page-title">Descubre nuestros productos más populares</h2>
          <ul className="popular-list">
            { popular.map((movie, index) =>
              <li key={`popular-${index}`} className="popular-item">
              <CardProduct
                productId={movie.id}
                title={movie.title}
                overview={movie.overview}
                releasedOn={movie.released_on}
                vote={movie.vote}
                categories={movie.category_ids}
              />
            </li>
            )}
          </ul>
        </div>
      </div>
    </PageWrapper>
  )
}

export default Home;
