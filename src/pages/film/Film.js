import { useState, useEffect  } from 'react'
import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import Breadcrumb from '../../components/common/Breadcrumb/CommonBreadcrumb'
import CardDetail from '../../components/card/Detail/CardDetail'
import { getMovieDetail } from '../../services/movies'
import { useParams } from 'react-router'

const Film = () => {
  const { productId } = useParams()
  const [detail, setDetail] = useState([])

  const breadcrumbs = [
    {
      title: 'Catalogo',
      to: '/catalogo'
    },
    {
      title: 'Peliculas'
    }
  ]
  async function fetchMovieDetail(id) {
    setDetail(await getMovieDetail(id))
  }
  useEffect(() => {
    fetchMovieDetail(productId)
  })
  return (
    <PageWrapper>
      <div className="section-wrapper">
        <div className="wrapper">
          <Breadcrumb breadcrumbs={breadcrumbs} />
          <h2 className="page-title">{ detail.title }</h2>
          <CardDetail
            productId={detail.id}
            title={detail.title}
            overview={detail.overview}
            releaseOn={detail.released_on}
            vote={detail.vote}
            categories={detail.categories}
            adults={detail.adults}
            productionCountries={detail.production_countries}
            revenue={detail.revenue}
            status={detail.status}
          />
        </div>
      </div>
    </PageWrapper>
  )
}

export default Film
