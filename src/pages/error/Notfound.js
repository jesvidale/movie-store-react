import { Link } from 'react-router-dom'
import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import './Notfound.scss'

const Notfound = () => {
  return (
    <PageWrapper>
      <div className="section-wrapper">
        <div className="wrapper">
          <h2 className="page-title sr-only">Peliculas de Accion</h2>
          <div className="notfound-wrapper">
            <img
              className="notfound-image"
              alt="OOPS"
              src={'/oops.png'}
            />
            <span className="notfound-title">404 - PAGE NOT FOUND</span>
            <div className="notfound-description">
              <span>The page you are looking for might have been removed had its name changed or is temporarily unavailable</span>
            </div>
            <Link className="button button-primary notfound-button" to="/">GO TO HOMEPAGE</Link>
          </div>
        </div>
      </div>
    </PageWrapper>
  )
}

export default Notfound
