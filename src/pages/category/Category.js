import { useState, useEffect  } from 'react'
import { useSelector } from 'react-redux'
import {  useParams } from 'react-router'

// components
import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import Breadcrumb from '../../components/common/Breadcrumb/CommonBreadcrumb'
import CardProduct from '../../components/card/Product/CardProduct'
import './Category.scss';

// services
import { getByGenre } from '../../services/movies'

const Category = () => {
  const breadcrumbs = [
    {
      title: 'Catalogo',
      to: '/catalogo'
    },
    {
      title: 'Peliculas'
    }
  ]
  const categoriesDictionary = useSelector(state => state.categories.list)
  const { categoryId } = useParams()
  const [movies, setMovies] = useState([])

  const getCategoryName = categoryID => {
    const res = categoriesDictionary.find(el => el.id === parseInt(categoryID))
    return res ? res.name: 'uncategorized'
  }
  
  async function fetchMovies(categoryID) {
    setMovies(await getByGenre(categoryID))
  }
  useEffect(() => {
    fetchMovies(categoryId)
  }, [categoryId])

  return (
    <PageWrapper>
      <div className="section-wrapper">
        <div className="wrapper">
        <Breadcrumb breadcrumbs={breadcrumbs} />
          <h2 className="page-title">Peliculas {categoryId ? `de ${getCategoryName(categoryId)}`: ''}</h2>
          <ul className="genre-list">
            {movies.map((movie, key) =>
            <li key={`popular-${key}`} className="genre-item">
              <CardProduct
                productId={movie.id}
                title={movie.title}
                overview={movie.overview}
                releasedOn={movie.released_on}
                vote={movie.vote}
                categories={movie.category_ids}
              />
            </li>
            )}
          </ul>
        </div>
      </div>
    </PageWrapper>
  )
}

export default Category
