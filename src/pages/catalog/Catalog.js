import { useState } from 'react'
import { useSelector } from 'react-redux'

// components
import PageWrapper from '../../containers/PageWrapper/PageWrapper'
import Breadcrumb from '../../components/common/Breadcrumb/CommonBreadcrumb'
import SearchForm from '../../components/search/Form/SearchForm'
import CardProduct from '../../components/card/Product/CardProduct'
import CardCategory from '../../components/card/Category/CardCategory'
import './Catalog.scss';

// services
import { getMovieByQuery } from '../../services/movies'

const Catalog = () => {
  const categories = useSelector(state => state.categories.list)
  const breadcrumbs = [
    {
      title: 'Catalogo'
    }
  ]
  const [searchResults, setSearchResults ] = useState([])
  const [query, setQuery] = useState('')

  const getSearchResults = async (query, e) => {
    e.preventDefault()
    setSearchResults(await getMovieByQuery(query))
  }

  return (
    <PageWrapper>
      <div className="section-wrapper">
        <div className="wrapper">
          <Breadcrumb breadcrumbs={breadcrumbs} />
          <h2 className="page-title">Catálogo</h2>
          <SearchForm
            query={query}
            onQueryChange={setQuery}
            triggerSearch={getSearchResults}
          >
            <h3 className="page-subtitle">Buscar película</h3>
          </SearchForm>
          <SearchResultsTemplate searched={searchResults}/>
        </div>
      </div>
      <div className="section-wrapper section-category">
        <div className="wrapper">
          <h3 className="page-subtitle">Buscar por genero</h3>
          <ul className="genres-list">
            { categories.map(({id, name}, key) =>
              <li key={`genre-${key}`} className="genres-item">
              <CardCategory
                categoryId={id}
                categoryTitle={name}
              />
            </li>
            )}
          </ul>
        </div>
      </div>
    </PageWrapper>
  )
}

const SearchResultsTemplate = ({searched}) =>  {
  return searched.length && searched !== undefined? (
    <div className="searched-results">
      <span className="searched-results-title">Resultados de la búsqueda</span>
      <ul className="searched-list">
        { searched.map((movie, index) =>
        <li key={`searched${index}`} className="searched-item">
          <CardProduct
            productId={movie.id}
            title={movie.title}
            overview={movie.overview}
            releasedOn={movie.released_on}
            vote={movie.vote}
            categories={movie.category_ids}
          />
        </li>
        )}
      </ul>
    </div>
  ): ''
}

export default Catalog;
