import { Categories, MovieResume, MovieDetail } from '../models/movies'
import { api } from './api'

export const getCategories = async () => {
  const resp = await api.requestCategories()
  return new Categories(resp.genres).orderByName
}

export const getMostPopular = async () => {
  const resp = await api.requestMostPopular()
  const results = resp.results.slice(0, 4)
  return results.map(el => new MovieResume(el))
}

export const getByGenre = async (genreID) => {
  const resp = await api.requestByGenre(genreID)
  return resp.results.map(el => new MovieResume(el))
}

export const getMovieDetail = async (movieID) => {
  const resp = await api.requestMovieDetail(movieID)
  return new MovieDetail(resp)
}

export const getMovieByQuery = async (query) => {
  const resp = await api.requestMovieByQuery(query)
  const results = resp.results.slice(0, 20)
  return results.filter(result => Object.prototype.hasOwnProperty.call(result, 'title') && result.overview !== '').map(el => new MovieResume(el))
}
