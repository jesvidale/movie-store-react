import { getCategories } from '../services/movies'

const storeCategories = categories => ({
  type: 'REFRESH_CATEGORIES',
  categories
})

export const getAllCategories = () => async dispatch => {
  const categories = await getCategories()
  dispatch(storeCategories(categories))
}
