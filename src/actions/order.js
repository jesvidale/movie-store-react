export const addToOrder = item => ({
  type: 'ADD_TO_ORDER',
  item
})

export const decreaseOrder = item => ({
  type: 'DECREASE_EXISTING_ORDER',
  item
})

export const removeFromOrder = item => ({
  type: 'REMOVE_FROM_ORDER',
  item
})
