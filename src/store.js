import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers'

const middleware = [ thunk ];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

/* eslint-disable no-underscore-dangle */
const store = createStore(
  reducer,
  composeEnhancer((applyMiddleware(...middleware)))
)
/* eslint-enable */

export default store
